import apache_beam as beam
import tensorflow_data_validation as tfdv
from tfx_bsl.public import tfxio
import pyarrow
from google.cloud import storage


## Util functions 
def row_to_nparray(row: Dict):
    return {k: np.asarray([v]) for k,v in row.items()}


def _convert_dict(legacy_examples: Dict) -> Dict:
    result = {}
    
    ##Tfdv doesn't support datetime hence converting it to string
    for k, v in legacy_examples.items():
        if np.issubdtype(v.dtype, np.number):
            result[k] = v
        else:
            result[k] = v.astype(str)
    return result

def write_to_gcs(buffer):
    client = storage.Client()
    bucket = client.get_bucket( "<gcs-bucket-name>" )
    blob = bucket.blob( "<local-file-name>" )
    blob.upload_from_filename("<gcs-bucket-name>>")
    

## Define arguments
pipeline_options = beam.pipeline.PipelineOptions.from_dictionary({'project': '<gcp-project-name>'})

args = ['--temp_location=gs://<gcs-bucket-name>']


##Building the pipeline
with beam.Pipeline(argv=args,options=pipeline_options) as p:
     r = (p 
            | 'BQ read' >> beam.io.Read(beam.io.ReadFromBigQuery(query='SELECT * FROM `<gcs-project-name>.<dataset-name>.<table-name>`',
                                                    use_standard_sql=True, gcs_location="gs://<gcs-bucket-name>"))
            | 'to dict' >> beam.Map(row_to_nparray)
            | 'format' >> beam.Map(_convert_dict)
            | 'batch' >> batch_util.BatchExamplesToArrowRecordBatches()
            | 'tdfv' >> tfdv.GenerateStatistics()
            | 'WriteStatsOutput' >> tfdv.WriteStatisticsToTFRecord(output_path = "<local-file-name>",sharded_output=False)
            | 'WriteOutputToGCS' >> beam.Map(write_to_gcs)
     )

stats = tfdv.load_statistics("<local-file-name>")
tfdv.visualize_statistics(stats)

## If you need to download 

# Create a bucket object for our bucket
bucket = client.get_bucket("<gcs-bucket-name>")
# Create a blob object from the filepath
blob = bucket.blob("<gcs-bucket-name>")
# Download the file to a destination
blob.download_to_filename("<file-name-to-save>")

